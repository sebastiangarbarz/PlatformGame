using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [SerializeField] private CharacterController2D controller;
    [SerializeField] private GameObject player; 

    float horizontalMove = 0f;
     


    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxis("Horizontal"); /* -1 to 1 */
        if (Input.GetKeyDown(KeyCode.Space)) {
            controller.Jump();
        }
        
    }
    void FixedUpdate() /* Movement */ 
    {
        

        controller.Move(horizontalMove);
        // gameManager.Move(horizontalMove * Time.fixedDeltaTime, false, false); /* false not crouch, false not jump */

    }
 

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    #region Serialized Fields
    [SerializeField] private CharacterController2D player = null;
    
    [SerializeField] private GameObject[] initialFloors = null;
    [SerializeField] private GameObject[] prefabPlatforms = null;
    
    [SerializeField] private GameObject mainScene = null;
    [SerializeField] private GameObject prefabBackground = null; /* Desde -2.25 de alto hasta -12.25 (el piso) */
    [SerializeField] private GameObject prefabSceneComing = null;
    [SerializeField] private float speed = 1.5f;
    [SerializeField] private Floor floor = null;
    [SerializeField] private GameObject lineOfRespawn = null;
    #endregion
    #region private variables
    private float backgroundWidth = 0f;
    private float backgroundHeight = 0f;
    private float heightToReach = 1.0f;
    private bool moving, moveFloors = false;
    private bool first = true;
    private GameObject background = null;
    private Vector3 targetPos = new Vector3();
    private List<GameObject> platformlist, floorsList;
    #endregion

    #region Unity Classes
    void Awake()
    {
        setBackground();
        targetPos = lineOfRespawn.transform.position;
        platformlist = new List<GameObject>();
        floorsList = new List<GameObject>();
        for (int i = 0; i < initialFloors.Length; i++)
        {
            floorsList.Add(initialFloors[i]);
        }
    }
    void Start()
    {
        instantiatePlatoforms(mainScene);
        spawnScreen();
    }

    // Update is called once per frame
    void Update()
    {
    
        if (moving) {
            moveBackgroundWithPlatforms();
        }
        if (moveFloors)
        {
            moveInitialFloors();
        }
       
    }
    #endregion

    #region Public Methods
    public void setBackground() {
        GameObject newBackground = new GameObject();
        GameObject newPrefabBackground= new GameObject();
      //  backgroundWidth = prefabBackground.GetComponent<SpriteRenderer>().sprite.texture.width;
      //  backgroundHeight = prefabBackground.GetComponent<SpriteRenderer>().sprite.texture.height;
        background = new GameObject();
    }


    public void spawnScreen() {
        setBackground();
        player.PassedHeight = true;
        if (player.PassedHeight) /* Ya puedo empezar a mover la escena */  
        { 
            background = Instantiate(prefabSceneComing, new Vector3(0, 7, 0), Quaternion.identity);
            background.GetComponent<SpriteRenderer>().enabled = false;
            moving = true;
            instantiatePlatoforms(background);
        }
       
    }
  
 
    public void instantiatePlatoforms(GameObject currBackground) {
        for (int i = 0; i < prefabPlatforms.Length; i++) /* Por cada prefab platform lo adhiero a la lista */
        {
            
            /* La altura de mis platforms tiene que ser desde medio Y hasta 1 y medio y) */ 
            float desdeY = currBackground.transform.position.y * 1 / 2;
            float hastaY = currBackground.transform.position.y * 3 / 2;
            float rndmY = Random.Range(desdeY, hastaY);
            float rndmX = Random.Range(-7.5f, 7.5f);
            Debug.Log("Mi x para la platform:" +rndmX);
            GameObject currPlatform = Instantiate(prefabPlatforms[i], new Vector3(rndmX, rndmY, 0), Quaternion.identity, currBackground.transform); /*No puedo parentear un prefab */ 
            //currPlatform.transform.SetParent(currBackground.transform);
            Debug.Log("Plataforma en x: " + currPlatform.transform.position.x );
            platformlist.Add(currPlatform);
            moving = true;
        }
    }
    public void moveBackgroundWithPlatforms() {
       
        for (int i = 0; i < platformlist.Count; i++) /* Check for destroy */
        {
            Vector3 fromPlatformToRespawnLine = new Vector3(platformlist[i].transform.position.x, lineOfRespawn.transform.position.y);
            platformlist[i].transform.position = Vector3.MoveTowards(platformlist[i].transform.position, fromPlatformToRespawnLine, speed * Time.deltaTime); /*No puedo parentear un prefab */
            Debug.Log("El i en el for vale: "+i+" y la lista tiene "+ platformlist.Count +" lugares");
            if (platformlist[i].transform.position.y <= lineOfRespawn.transform.position.y)
            {
                GameObject localGameObject = platformlist[i];
                Destroy(localGameObject);
                platformlist.RemoveAt(i);
                Debug.Log("Removi en "+i+" y ahora el size de mi lista es "+platformlist.Count);
            
                
            }
        } /* Se mueve el background y por ende sus plataformas hijas */
        Vector3 backgroundFinalPosition = new Vector3(background.transform.position.x, -13); /* Que vaya hasta abajo del floor */
        background.transform.position = Vector3.MoveTowards(background.transform.position, backgroundFinalPosition, speed * Time.deltaTime); /*No puedo parentear un prefab */

        if (platformlist.Count <= 0) { /* Si no hay m�s plataformas tengo que spawnear otra screen */
            spawnScreen();
            moveFloors = true; /* No hay m�s plataformas: el player est� subido a alguna: los floors tienen que caer. */ 
        }
    }
    public void moveInitialFloors() {
                
                for (int i = 0; i < floorsList.Count; i++) /* Se empiezan a mover */
                {
                 Vector3 fromFloorToRespawnLine = new Vector3(floorsList[i].transform.position.x, lineOfRespawn.transform.position.y);
                 floorsList[i].transform.position = Vector3.MoveTowards(floorsList[i].transform.position, fromFloorToRespawnLine, speed * Time.deltaTime); /*No puedo parentear un prefab */
                    if (floorsList[i].transform.position.y <= lineOfRespawn.transform.position.y) 
                    {
                        GameObject localGameObject = floorsList[i];
                        Destroy(localGameObject);
                        floorsList.RemoveAt(i);
                    } 
                   
                }
    }

    
    public void floorCollision(GameObject collided) {
        if (collided.transform.position.y <= 5.5f) {  /* reached the floor */
            if (collided.tag == "platform")
            {
                Destroy(collided);
                
            }
        }
    }

    public void ResizeSpriteToScreen(GameObject GOspriteBack)
    {
        /* Chequear si existe el Sprite Render 
        var sr = GetComponent(SpriteRenderer);
        if (sr == null) return;
        */
        SpriteRenderer sr = GOspriteBack.GetComponent<SpriteRenderer>();


        float width = sr.sprite.bounds.size.x;
        float height = sr.sprite.bounds.size.y;

        float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
        float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
        /* Debug.Log("La camara altura : " + worldScreenHeight);
         Debug.Log("La camara ancho : " + worldScreenWidth);
         Debug.Log("la pantalla alto:  "+ Screen.height);
         Debug.Log("la pantalla ancho:  " + Screen.width);
         Debug.Log("cambio tama�o de sprite a: "+worldScreenWidth / width +" y: " + worldScreenHeight / height);*/
        GOspriteBack.transform.localScale.Set((worldScreenWidth / width), (worldScreenHeight / height), 0);

    }
    public void switchMovement()
    {
        if (moving == true) { moving = false; return; }
        if (moving == false) { moving = true; return; }
    }
    public void startMovement()
    {
        moving = true;
    }
    public void stopMovement()
    {
        moving = false;
    }
    public void setBackgroundScale() {

        ResizeSpriteToScreen(prefabBackground);
    }
    public float getSceneHeight()
    {
        return prefabBackground.transform.localScale.y;

    }
    public float getSceneWidth()
    {
        return prefabBackground.transform.localScale.x;

    }
    #endregion
}

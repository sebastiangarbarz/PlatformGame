using UnityEngine;
using UnityEngine.Events;

public class CharacterController2D : MonoBehaviour
{
    #region serialized fields
    [SerializeField] private float runSpeed = 1.0f;
    [SerializeField] private Rigidbody2D _rigidBody;
    [SerializeField] private BoxCollider2D _BoxCollider2D;
    [SerializeField] private float JumpForce = 10.0f;
    [SerializeField] private Animator animator;
    [SerializeField] private Transform groundCheck;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private float layerRadius = 0.5f;
    #endregion
    #region private variables
    private bool m_FacingRight = true;
    private bool passedHeight = false;
    private bool hasJumped = false;
    private bool isGrounded;

    public bool PassedHeight { get => passedHeight; set => passedHeight = value; }
    #endregion
    public void Awake()
    {
        
    }
    public void Start()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _BoxCollider2D = GetComponent<BoxCollider2D>();
      
    }
    public void Update()
    {
        
    }
    public void FixedUpdate()
    {
        animator.SetFloat("jump", _rigidBody.velocity.y);
        Debug.Log("Groundcheck pos: " + groundCheck.position);
        Debug.Log("Groundlayer  " + groundLayer);

        isGrounded = Physics2D.OverlapCircle(groundCheck.position, layerRadius, groundLayer);
    }
    public void Move(float towardsWhere) {
        
        _rigidBody.velocity += new Vector2(towardsWhere*runSpeed*Time.deltaTime, 0);
       
        animator.SetFloat("speed", _rigidBody.velocity.x);
        // If the input is moving the player right and the player is facing left...
        if (towardsWhere > 0 && !m_FacingRight)
        {
            // ... flip the player.
            //Flip();
        }
        // Otherwise if the input is moving the player left and the player is facing right...
        else if (towardsWhere < 0 && m_FacingRight)
        {
            // ... flip the player.
            //Flip();
        }

    }

    public void Jump() {
        Debug.Log(isGrounded);
        if (isGrounded) /* Solo salto 1 vez */ {
            _rigidBody.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
            hasJumped = true;
            if (Mathf.Abs(_rigidBody.velocity.y) > 2) /* Pas� la altura para empezar a mover el juego */
            {
                PassedHeight = true;
            }
           
        }
        
    }
    public void checkStanding() {
      
    }

    private void Flip()
    {
        // Switch the way the player is labelled as facing.
        //m_FacingRight = !m_FacingRight;

        // Multiply the player's x local scale by -1.
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}